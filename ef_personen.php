<?php
/*
Plugin Name: EF Personen
Description: Modifikationen und Ergänzungen für das Our Team Plugin.
Author: Timo Klemm
Version: 0.3.1
Author URI: https://github.com/team-ok
*/

add_action('after_setup_theme', function(){
	if ( ! defined('BEANS_FRAMEWORK_AVAILABLE') ){
		define( 'BEANS_FRAMEWORK_AVAILABLE', function_exists( 'beans_load_document' ) );
	}
});

require_once( plugin_dir_path( __FILE__ ) . 'ef_personen_taxonomies.php' );

require_once( plugin_dir_path( __FILE__ ) . 'ef_personen_fields.php' );

require_once( plugin_dir_path( __FILE__ ) . 'ef_personen_styles.php' );

require_once( plugin_dir_path( __FILE__ ) . 'ef_personen_scripts.php' );

require_once( plugin_dir_path( __FILE__ ) . 'ef_personen_class.php' );

require_once( plugin_dir_path( __FILE__ ) . 'ef_personen_shortcode.php' );


// modify our team single slug
add_filter( 'woothemes_our_team_single_slug', function($slug){

	$slug_mod = get_theme_mod( 'ef_team_member_slug' );
	if ( $slug_mod ){
		return $slug_mod;
	}

	return $slug;

});

// disable our team nav menu option and change labels to 'personen'
add_filter( 'woothemes_our_team_post_type_args', function($args){

	$args['show_in_nav_menus'] = false;

	$args['labels'] = array(
		'name' 					=> 'Personen',
		'singular_name' 		=> 'Person',
		'add_new' 				=> 'Neu hinzufügen',
		'add_new_item' 			=> 'Neue Person hinzufügen',
		'edit_item' 			=> 'Person bearbeiten',
		'new_item' 				=> 'Neue Person',
		'all_items' 			=> 'Alle Personen',
		'view_item' 			=> 'Person anzeigen',
		'search_items' 			=> 'Personen suchen',
		'not_found' 			=> 'Keine Personen gefunden',
		'not_found_in_trash' 	=> 'Keine Person im Papierkorb gefunden',
		'parent_item_colon' 	=> '',
		'menu_name' 			=> 'Personen'

	);

	return $args;
});

// add content to single and archive views
add_filter( 'the_content', 'ef_personen_add_content_to_single_and_archive', 5);
function ef_personen_add_content_to_single_and_archive($content){
	if ( 'team-member' != get_post_type() ){
		return $content;
	}
	$ef_person = new EF_Personen();
	return $ef_person->person_info() . $content;
}