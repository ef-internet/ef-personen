<?php

class EF_Personen {

	private $atts;


	public function __construct( $atts = array() ){

		$this->atts = shortcode_atts( array(

			'limit' 					=> 12,
			'per_row' 					=> null,
			'orderby' 					=> 'title',
			'order' 					=> 'ASC',
			'id' 						=> 0,
			'slug'						=> null,
			'display_author' 			=> true,
			'display_additional' 		=> true,
			'display_avatar' 			=> true,
			'display_url' 				=> false,
			'display_author_archive'	=> false,
			'display_twitter' 			=> false,
			'display_role'	 			=> true,
			'effect' 					=> 'fade', // Options: 'fade', 'none'
			'pagination' 				=> false,
			'echo' 						=> true,
			'size' 						=> 250,
			'category' 					=> 0,
			'title'						=> '',
			'before_title' 				=> '<h2>',
			'after_title' 				=> '</h2>',
			'grid'						=> 'static',
			'showall_label'				=> 'Alle',
			'gutter'					=> 15,
			'default_filter'			=> '\'\'', // = empty = all
			'show_modal'				=> 'false',
			'show_logo'					=> 'false',
			'show_content'				=> 'false',
			'columns'					=> '6',
			'columns_max' 				=> isset($atts['columns']) ? $atts['columns'] : '6',
			'columns_med'				=> isset($atts['columns']) && is_numeric($atts['columns']) ? ceil($atts['columns'] / 2) : '3',
			'columns_min' 				=> isset($atts['columns']) && is_numeric($atts['columns']) ? ceil($atts['columns'] / 4) : '2',


		), $atts );

		// fix integer
		$this->atts['size'] = is_numeric( $this->atts['size'] ) ? intval( $this->atts['size'] ) : 250;

		// fix boolean
		$this->atts['show_content'] = $this->atts['show_content'] === 'true' ? true : false;
		$this->atts['show_modal'] = $this->atts['show_modal'] === 'true' && $this->atts['show_content'] ? true : false;
		$this->atts['show_logo'] = $this->atts['show_logo'] === 'true' ? true : false;

	}

	
	public function filter_single_html($html, $post){

		// insert (visual) modal trigger icon after title
		if ( $this->atts['show_modal'] && !empty( $post->post_content ) ):

			$search = $post->post_title . '</h3>';
			$replace = $post->post_title . '<span class="pulse"><i class="uk-icon-plus-circle"></i></span></h3>';
			$start = strpos($html, $search);
			if ( $start !== false ){
				$html = substr_replace( $html, $replace, $start, strlen($search) );
			}

		endif;

		// add filter names to team members html output
		if ( $this->atts['grid'] === 'dynamic' ):

			// get associated filter categories
			$cat = wp_get_post_terms( $post->ID, 'speaker_filter' );
			$filter_list = array();

			if ( ! is_wp_error( $cat ) && ! empty( $cat ) ):

				foreach ( $cat as $filter ) {

					// add filter name to array
					$filter_list[] = $filter->term_id;

				}

			endif;

			// create comma seperated string of filter names
			$filter_list = implode(',', $filter_list);

			// wrap team-member div and add data attribute with filter names
			$html = '<div data-uk-filter="'.esc_attr($filter_list).'">' . $html . '</div>';

		else: // non-dynamic grid
			$grid_column_classes = apply_filters( 'ef_personen_standard_grid_column_class', 'uk-position-relative', $this->atts );
			$wrapper_classes = apply_filters( 'ef_personen_standard_grid_wrapper_class', 'uk-panel uk-panel-box', $this->atts );
			$_html = '<div class="' . esc_attr( $grid_column_classes ) . '">' . "\n";
			$_html .= '<div class="' . esc_attr( $wrapper_classes ) . '">' . "\n" . $html . "\n" . '</div>' . "\n";
			$_html .= '</div>';
			$html = $_html;

		endif;

		if ( $this->atts['show_modal'] && !empty( $post->post_content ) ):

			// add modal trigger to html (right before closing </div>)
			$html = substr_replace( $html, '<a class="uk-position-cover uk-position-z-index" href="#ef-speaker-content-' . $post->ID . '" data-uk-modal></a>'. "\n" . '</div>' . "\n", -6, 6);

		endif;

		return $html;
	}

	// insert speaker infos after title
	public function person_info($html = ''){
		
		$speaker_function = get_field( 'ef-speaker-function' );
		$speaker_partner = get_field( 'ef-speaker-partner' );
		$speaker_company = get_field( 'ef-speaker-company' );
		$_html = '<div class="ef-speaker-info">';

		if ( $speaker_function ){
			$_html .= '<span class="ef-speaker-position">' . $speaker_function . '</span>';			
		}

		if ( ! empty($speaker_partner) && $speaker_partner instanceof WP_Post ):

			$name = $speaker_company ? $speaker_company : $speaker_partner->post_title;

			$_html .= '<span class="ef-speaker-company">' . esc_html($name) . '</span>';

			if ( $this->atts['show_logo'] ):

				$logo = get_field( 'partner-logo', $speaker_partner->ID ); // array
				$link = get_field( 'partner-url', $speaker_partner->ID );

				if ( ! empty( $logo ) ):
					$_html .= '<a class="ef-speaker-company-link" href="' . esc_url($link) . '" target="_blank" rel="noopener nofollow"><img src="' . $logo['sizes']['medium'] . '" width="' . $logo['sizes']['medium-width'] . '" height="' . $logo['sizes']['medium-height'] . '" alt="' . esc_attr($name) . '"></a>';
				endif;	
			
			endif;

		elseif ( $speaker_company ):

			$_html .= '<span class="ef-speaker-company">' . esc_html($speaker_company) . '</span>';

		endif;

		$_html .= '</div>';

		$html = $_html . $html;

		return $html;
	}

	// filter our team html output
	public function filter_all_html($html, $query, $args){

		$wrapper_classes = 'ef-personen';
		$grid_classes = '';
		$grid_attributes = '';
		$nav = '';

		if ( $this->atts['show_modal'] ){
			$grid_classes .= ' ef-modal';
		}

		if ( $this->atts['grid'] === 'dynamic' ):

			$wrapper_classes .= ' dynamic-grid';

			/*----------  add the speaker filter navigation  ----------*/

			$speaker_ids = array();
			// get all speaker ids from our team query
			foreach ($query as $speaker) {
				$speaker_ids[] = $speaker->ID;
			}

			// get terms (filter id/name pair) limited to those associated with the currently queried speakers
			$filter = get_terms( array( 
				'taxonomy' => 'speaker_filter',
				'fields' => 'id=>name',
				'object_ids' => $speaker_ids
			) );

			// html output of filter navigation
			$args_hash = '';
			if ( ! is_wp_error( $filter ) && ! empty( $filter ) ):
				$args_hash = substr( md5( serialize( $this->atts ) ), 0, 8 ); // create hash value of $this->atts array to be used as dynamic filter id
				$nav .= '<ul id="ef-personen-filter-'. $args_hash .'" class="uk-subnav uk-subnav-pill ef-dynamic-grid-filter" data-uk-sticky="{boundary:true}">' . "\n";
				$nav .= '<li data-uk-filter=""><a href>'. esc_html( $this->atts['showall_label'] ) .'</a></li>' . "\n";
					foreach ( $filter as $id => $name ) {
						$nav .= '<li data-uk-filter="' . $id .'"><a href>' . $name . '</a></li>' . "\n";
					}
				$nav .= '</ul>' . "\n";

			endif;

			$grid_classes .= ' uk-grid uk-grid-width-1-' . $this->atts['columns_min'] . ' uk-grid-width-medium-1-' . $this->atts['columns_med'] . ' uk-grid-width-xlarge-1-' . $this->atts['columns_max'];

			$grid_attributes .= ' data-uk-grid="{\'gutter\':' . $this->atts['gutter'] . ', \'controls\': \'#ef-personen-filter-' . $args_hash . '\', \'filter\':' . $this->atts['default_filter'] . '}"';

			$grid_classes = apply_filters( 'ef_personen_dynamic_grid_classes', $grid_classes, $this->atts );

			$grid_attributes = apply_filters( 'ef_personen_dynamic_grid_attributes', $grid_attributes, $this->atts );

		else:

			$wrapper_classes .= ' static-grid';
			$grid_classes .= ' uk-grid uk-grid-width-1-' . $this->atts['columns_min'] . ' uk-grid-width-medium-1-' . $this->atts['columns_med'] . ' uk-grid-width-xlarge-1-' . $this->atts['columns_max'] . ' uk-grid-match uk-grid-small';
			$grid_classes = $this->atts['columns'] === '1' ? $grid_classes . ' ef-personen-full-width' : $grid_classes;

/*			if ( count( $query ) > $this->atts['columns'] ) {
				$grid_classes .= ' uk-flex-center';
			}*/

			$grid_attributes .= ' data-uk-grid-margin';

			$grid_classes = apply_filters( 'ef_personen_standard_grid_classes', $grid_classes, $this->atts );

			$grid_attributes = apply_filters( 'ef_personen_standard_grid_attributes', $grid_attributes, $this->atts );

		endif;
			
		$html = preg_replace( '/class="team-members[^"]+/', '$0' . $grid_classes, $html );

		$html = preg_replace( '/class="team-members[^>]+/', '$0' . $grid_attributes, $html );

		// filter wrapper classes
		$wrapper_classes = apply_filters( 'ef_personen_wrapper_classes', $wrapper_classes, $this->atts );

		// wrap html output in a div
		$html = '<div class="' . esc_attr( $wrapper_classes ) . '">' . "\n" . $nav . $html . "\n" . '</div>';

		// enqueue frontend assets
		if ( ! BEANS_FRAMEWORK_AVAILABLE ){
			wp_enqueue_script( 'uikit' );
			wp_enqueue_script( 'uikit-grid' );
			wp_enqueue_script( 'uikit-sticky' );

			wp_enqueue_style( 'uikit' );
			wp_enqueue_style( 'uikit-sticky' );
			wp_enqueue_style( 'ef-personen' );
		}

		return $html;
	}

	
	public function filter_single_content($content, $post){

		if ( ! $this->atts['show_content'] ){
			return '';
		}

		// wrap content in modal
		// add speaker name to content
		if ( $this->atts['show_modal'] && !empty( $post->post_content ) ):

			$image = get_the_post_thumbnail( $post->ID, array( $this->atts['size'], $this->atts['size'] ) );
			$info = $this->person_info('');

			$modal_content = '<div id="ef-speaker-content-'. $post->ID .'" class="uk-modal">' . "\n";
			$modal_content .= '<div class="uk-modal-dialog">' . "\n";
			$modal_content .= '<a href="#" class="uk-modal-close uk-close"></a>' . "\n";
			$modal_content .= $image . "\n";
			$modal_content .= '<h3>' . esc_html($post->post_title) . '</h3>' . "\n";
			$modal_content .= $info . "\n";
			$modal_content .= $content . "\n";
			$modal_content .= '</div>'. "\n";
			$modal_content .= '</div>'. "\n";

			return $modal_content;

		endif;

		return $content;
	}

	public function get_html(){

		if ( ! function_exists( 'woothemes_our_team_shortcode' ) ){

			return '';
		}

		// add filter names to team members html output
		add_filter( 'woothemes_our_team_member_html', array( $this, 'filter_single_html' ), 10, 2 );

		// insert speaker infos after title
		add_filter( 'woothemes_our_team_member_fields_display', array( $this, 'person_info' ) );

		// filter our team html output to add the speaker filter navigation
		add_filter( 'woothemes_our_team_html', array( $this, 'filter_all_html' ), 10, 3 );
		
		// filter single content
		add_filter( 'woothemes_our_team_content', array( $this, 'filter_single_content' ), 10, 2 );

		$html = woothemes_our_team_shortcode( $this->atts );

		// remove filters after woothemes shortcode has run
		remove_filter( 'woothemes_our_team_member_html', array( $this, 'filter_single_html' ) );
		remove_filter( 'woothemes_our_team_member_fields_display', array( $this, 'person_info' ) );
		remove_filter( 'woothemes_our_team_html', array( $this, 'filter_all_html' ) );
		remove_filter( 'woothemes_our_team_content', array( $this, 'filter_single_content' ) );

		return $html;

	}

}