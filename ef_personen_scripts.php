<?php

// load speaker grid javascript
add_action( 'wp_enqueue_scripts', 'ef_personen_scripts' );

function ef_personen_scripts(){
	wp_enqueue_script( 'ef-speaker', plugin_dir_url( __FILE__ ) . 'scripts/ef-speaker.js', array( 'jquery' ) );

}