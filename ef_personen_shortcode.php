<?php

// wrapper for the woothemes our team shortcode
add_shortcode( 'ef_personen', 'ef_get_personen' );
function ef_get_personen( $atts ){

	$ef_person = new EF_Personen( $atts );

	return $ef_person->get_html();
}