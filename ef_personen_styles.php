<?php

add_action( 'beans_uikit_enqueue_scripts', function(){
	global $_beans_uikit_enqueued_items;

	if ( ! is_array( $_beans_uikit_enqueued_items ) ){
		return;
	}

	$dependencies = array( 
		'core' => array( 
			'animation', 'cover', 'subnav', 'switcher', 'modal'
		),
		'add-ons' => array(
			'grid', 'sticky'
		)
	);
	$core = array_diff( $dependencies['core'], $_beans_uikit_enqueued_items['components']['core'] );
	$add_ons = array_diff( $dependencies['add-ons'], $_beans_uikit_enqueued_items['components']['add-ons'] );

	if ( ! empty( $core ) ){

		beans_uikit_enqueue_components( $core );
	}

	if ( ! empty( $add_ons ) ){

		beans_uikit_enqueue_components( $add_ons, 'add-ons' );
	}

});

add_action( 'beans_uikit_enqueue_scripts', 'ef_personen_enqueue_less_fragment' );

function ef_personen_enqueue_less_fragment(){
	beans_compiler_add_fragment( 'uikit', plugin_dir_path( __FILE__ ) . '/styles/ef_personen.less', 'less' );
}

add_action('wp_enqueue_scripts', function(){

	if ( ! BEANS_FRAMEWORK_AVAILABLE ){
		wp_register_script( 'uikit', 'https://cdnjs.cloudflare.com/ajax/libs/uikit/2.27.5/js/uikit.min.js', array( 'jquery' ) );
		wp_register_style( 'uikit', 'https://cdnjs.cloudflare.com/ajax/libs/uikit/2.27.5/css/uikit.min.css' );
		wp_register_style( 'uikit-sticky', 'https://cdnjs.cloudflare.com/ajax/libs/uikit/2.27.5/css/components/sticky.min.css' );
		wp_register_script( 'uikit-sticky', 'https://cdnjs.cloudflare.com/ajax/libs/uikit/2.27.5/js/components/sticky.min.js' );
		wp_register_script( 'uikit-grid', 'https://cdnjs.cloudflare.com/ajax/libs/uikit/2.27.5/js/components/grid.min.js' );
		wp_register_style( 'ef-personen', plugin_dir_url( __FILE__ ) . 'styles/ef_personen.css' );
	}
});