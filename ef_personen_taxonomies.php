<?php
function register_ef_personen_filter() {

	/**
	 * Taxonomy: Themen-Filter.
	 */

	$labels = array(
		"name" => "Themen-Filter",
		"singular_name" => "Themen-Filter",
		"add_new_item" => "Neuen Filter hinzufügen",
		"all_items" => "Alle Themen-Filter",
		"edit_item" => "Themen-Filter bearbeiten",
		"view_item" => "Themen-Filter ansehen",
		"edit_item" => "Themen-Filter aktualisieren",
		"new_item_name" => "Name des neuen Themen-Filters",
		"not_found" => "Keine Themen-Filter gefunden"
	);

	$args = array(
		"label" => "Themen-Filter",
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => false,
		"hierarchical" => false,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'speaker_filter', 'with_front' => true, ),
		"show_admin_column" => true,
		"show_in_rest" => false,
		"rest_base" => "speaker_filter",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => true,
		);
	register_taxonomy( "speaker_filter", array( "team-member" ), $args );
}
add_action( 'init', 'register_ef_personen_filter' );